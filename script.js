/* class Settings */
class Settings{
  /* constructor */
  constructor(){
    // setup defaults
    this.setupDefaults();
    // setup asteroid pool data
    this.setupAsteroidPool();
  }


  /* setup defaults */
  setupDefaults(){
    // defaults
    this.run=false;                      // game status
    this.con=false;                      // game level (new/continue)
    this.brk=false;                      // game pause
    this.winw=0;                         // screen width
    this.winh=0;                         // screen height
    this.nln=String.fromCharCode(10);    // line break
    this.plrlifedef=1;                   // life amount default
    this.plrlifemax=6;                   // maximum amount of lifes
    this.plrlife=0;                      // life counter
    this.plrlifeaud='';                  // life audio
    this.plrpts=0;                       // score
    this.plrlvl=0;                       // level
    this.astamtdef=3;                    // asteroid amount default
    this.astamt=0;                       // asteroid amount in game
    this.astpool=[];                     // asteroid pool
    this.astlst=[];                      // active asteroids
    this.shtrng=80;                      // shot lifetime (range)
    this.shtmax=10;                      // shot maximum
    this.shtlst=[];                      // shot list
    this.shtdeldef=20;                   // shot delay default
    this.shtdel=0;                       // shot delay
    this.shtaud='';                      // shot audio
    this.shppx=0;                        // ship position x
    this.shppy=0;                        // ship position y
    this.shpspdx=0;                      // ship speed x
    this.shpspdy=0;                      // ship speed y
    this.shpspdlimit=8;                  // ship speed limit
    this.shpdeg=0;                       // ship rotation angle
    this.shpdegspd=10;                   // ship rotation speed
    this.shptmr=500;                     // wait to show ship
    this.spwnsz=180;                     // spawn area size
  }

  /* set the asteroid pool to choose from */
  setupAsteroidPool(){
    // type,points,shape
    this.astpool.push({
      'x':
      [
        ['x', 20,'19 31,8 54,14 64,10 71,35 87,24 94,33 104,65 91,78 104,110 96,116 69,113 46,88 43,75 34,60 35,50 19,40 13,19 28'],
        ['x', 20,'6 41,23 63,35 70,14 85,25 105,42 119,83 113,101 95,114 80,88 59,112 37,100 26,86 29,76 24,64 25,44 20,51 8,8 16'],
        ['x', 20,'27 26,24 23,25 26,10 42,16 71,22 76,6 82,45 92,31 113,68 117,94 107,100 99,83 65,108 75,113 48,93 17,60 11,21 22'],
        ['x', 20,'8 62,8 70,37 91,33 106,43 115,74 101,93 105,118 56,117 35,88 13,79 25,49 8,29 15,42 45'],
        ['x', 20,'5 44,12 76,30 91,14 114,42 115,59 99,78 120,73 87,93 110,113 76,104 65,116 43,110 39,60 53,101 29,94 19,75 12,37 23,33 8,15 16'],
        ['x', 20,'14 38,25 70,21 76,39 104,56 113,89 104,102 85,113 67,113 47,112 31,99 20,80 20,52 10,37 10,27 16,15 33'],
      ],
      'm':
      [
        ['m', 50,'10 49,8 56,23 57,20 68,48 61,58 69,74 51,75 40,82 36,74 24,50 11,42 12,33 10,8 34'],
        ['m', 50,'3 30,26 53,14 60,35 74,64 67,74 54,62 45,79 33,71 11,60 19,46 10,42 12,26 7,17 27'],
        ['m', 50,'10 35,5 49,15 59,22 73,47 79,64 74,78 58,76 39,73 14,60 8,35 9,29 14,19 9,12 11,5 17'],
        ['m', 50,'8 36,19 50,10 64,30 77,40 63,46 78,60 73,71 65,60 42,70 55,74 44,71 14,63 11,43 21,46 9,30 9,16 20'],
        ['m', 50,'8 62,33 80,63 76,76 63,78 42,78 19,52 8,24 6,8 18'],
        ['m', 50,'12 34,26 42,12 52,29 62,42 59,43 68,63 62,75 49,77 32,75 19,70 15,53 7,37 8,33 19,26 8,10 13'],
      ],
      's':
      [
        ['s',100,'5 18,6 33,19 39,35 33,39 19,20 7,12 9,5 14'],
        ['s',100,'10 17,10 27,14 31,28 35,37 24,35 17,25 9,17 9,8 14'],
        ['s',100,'2 24,14 30,16 35,26 34,33 25,29 16,24 13,18 19,16 12,15 10,8 11,6 17'],
        ['s',100,'8 11,8 20,14 31,17 36,27 35,33 33,31 17,26 13,16 8,5 9'],
        ['s',100,'4 19,4 27,18 34,26 28,31 17,30 11,25 6,12 3,6 6'],
        ['s',100,'8 16,6 28,15 31,12 35,25 32,23 26,31 26,33 13,27 4,23 3,17 20,19 7,15 7,8 13'],
      ]
    });
  }
}

/* class Tools */
class Tools{
  /* constructor */
  constructor(set){
    this.SET=set;
  }


  /* add class to element */
  addClassToElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      document.getElementById(elmid).className=newcls;
    }
  }

  /* remove class from element */
  removeClassFromElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    var newcls=cls.split(elmcls).join('');
    document.getElementById(elmid).className=newcls;
  }

  /* get the string with class names for element */
  getClassStringForElementById(elmid){
    return document.getElementById(elmid).className;
  }

  /* get inner html for element by id */
  getElementInnerHtmlById(elmid){
    return document.getElementById(elmid).innerHTML;
  }

  /* set inner html for element by id */
  setElementInnerHtmlById(elmid,str){
    document.getElementById(elmid).innerHTML=str;
  }

  /* get width/height for element by id */
  getSizeForElement(elmid,xy){
    var val=0;
    switch(xy){
      case 'x':
        val=parseInt(document.getElementById(elmid).offsetWidth);
        break;
      case 'y':
        val=parseInt(document.getElementById(elmid).offsetHeight);
        break;
    }
    return val;
  }

  /* set width/height (a '-' meand do not) for element by id */
  setSizeForElementById(elmid,px,py){
    if(isFinite(px)){document.getElementById(elmid).style.width=parseInt(px)+'px'};
    if(isFinite(px)){document.getElementById(elmid).style.height=parseInt(py)+'px'};
  }

  /* set position (a '-' means do not) for element by id */
  setPositonForElementById(elmid,px,py){
    if(isFinite(px)){document.getElementById(elmid).style.left=parseInt(px)+"px"};
    if(isFinite(py)){document.getElementById(elmid).style.top=parseInt(py)+"px"};
  }

  /* get x position for element by id */
  getXPositionForElementById(elmid){
    var val=parseInt(document.getElementById(elmid).style.left.replace('px',''));
    return isNaN(val)?0:val;
  }

  /* get y position for element by id */
  getYPositionForElementById(elmid){
    var val=parseInt(document.getElementById(elmid).style.top.replace('px',''));
    return isNaN(val)?0:val;
  }

  /* set rotation for element by id */
  setRotationForElementById(elmid,deg){
    document.getElementById(elmid).style.transform='rotate('+parseInt(deg)+'deg)';
  }

  /* get rotation for element by id */
  getRotationForElementById(elmid){
    return document.getComputedStyle(document.getElementById(elmid,null)).getPropertyValue('transform');
  }

  /* get visibility for element (class with 'show') */
  isElementVisibleById(elmid){
    return this.getClassStringForElementById(elmid).includes('show')==true;
  }

  /* hide cover layer */
  hideCover(){
    this.addClassToElementById('cover1','cover1hide');
    this.addClassToElementById('cover2','cover2hide');
  }

  /* center info boxes */
  centerInfoBoxes(){
    var px=parseInt((this.SET.winw/2)-120);
    var py=parseInt((this.SET.winh/2)-120);
    this.setPositonForElementById('start',px,py);
    var px=parseInt((this.SET.winw/2)-80);
    var py=parseInt((this.SET.winh/2)-40);
    this.setPositonForElementById('gameover',px,py);
  }

  /* format number with leading zero */
  numFormat(num,size){
    var str="000000000"+num;
    return str.substr(str.length-size);
  }

  /* convert degree to radiant */
  convertDegToRad(deg){
    return deg*(Math.PI/180.0);
  }

  /* remove item from array by key */
  removeItemFromArrayByKey(key,arr){
    arr.splice(key,1);
    return arr;
  }

  /* add item to array */
  addItemToArray(item,arr){
    arr.push(item);
    return arr;
  }

  /* play sound */
  playSound(snd){
    snd.cloneNode(true).play();
  }
}

/* class Sound */
class Sound{
  /* constructor */
  constructor(){
    this.setupAudio();
  }


  /* setup game audio files */
  setupAudio(){
    this.shpaud=new Audio('thrust.wav');
    this.shpaud.volume=0.10;
    this.shtaud=new Audio('shot.wav');
    this.shtaud.volume=0.75;
    this.expaud=new Audio('explosion.wav');
    this.expaud.volume=0.75;
    this.plrlifeaud=new Audio('levelup.wav');
    this.plrlifeaud.volume=0.75;
    this.hypaud=new Audio('hyper.mp3');
    this.hypaud.volume=0.5;
    this.btnaud=new Audio('button.wav');
    this.btnaud.volume=0.75;
  }
}

/* class Game */
class Game{
  /* constructor */
  constructor(){
    // objects
    this.SET=new Settings();
    this.TLS=new Tools(this.SET);
    this.SND=new Sound();
    // event listener
    addEventListener('click',this);
    addEventListener('keydown',this);
    addEventListener('contextmenu',this);
    addEventListener('wheel',this);
    addEventListener('resize',this);
  }


  /* draw asteroids */
  drawAsteroids(){
    // new html
    var html='';
    var key=0;
    this.SET.astlst.forEach((data)=>{
      html+='    <svg class="rock rock-'+data[0]+'" aspect-ratio="XminYmin" style="top:'+parseInt(data[4])+'px;left:'+parseInt(data[3])+'px;transform:rotate('+parseInt(data[7])+'deg);" attr-key="'+key+'" attr-type="'+data[0]+'" attr-pts="'+data[1]+'">'+this.SET.nln;
      html+='      <polygon points="'+data[2]+'"></polygon>'+this.SET.nln;
      html+='    </svg>'+this.SET.nln;
      key++;
    });
    // set html
    this.TLS.setElementInnerHtmlById('rocks',html);
  }

  /* move asteroids */
  moveAsteroids(){
    // walk list
    var key=0;
    this.SET.astlst.forEach((data)=>{
      // add speed
      var px=data[3];
      var py=data[4];
      var sx=data[5];
      var sy=data[6];
      px+=sx;
      py+=sy;
      // rotation
      var deg=data[7];
      var dir=data[8];
      deg+=dir;
      // come back on the mirrored (x/y) side
      px=this.getMirrorPosition(px,'x');
      py=this.getMirrorPosition(py,'y');
      // set data back to list
      data[3]=px;
      data[4]=py;
      data[7]=deg;
      this.SET.astlst[key]=data;
      key++;
    });
  }

  /* create asteroid data */
  createAsteroidData(item,from){
    // get basic params
    var type=item[0];
    var pts=item[1];
    var shp=item[2];
    // set position (from an asteroid or not)
    if(from==null){
      var px=Math.floor(Math.random()*this.SET.winw);
      var py=Math.floor(Math.random()*this.SET.winh);
    }else{
      var px=this.SET.astlst[from][3];
      var py=this.SET.astlst[from][4];
    }
    // set speed (randomly up/down/left/right)
    var spds=this.getRandomAsteroidSpeed();
    var sx=spds[0];
    var sy=spds[1];
    // degree and rotation speed
    var drs=this.getDegAndRotationSpeed();
    var deg=drs[0];
    var spd=drs[1];
    // return data
    return [type,pts,shp,px,py,sx,sy,deg,spd];
  }

  /* setup initial level asteroid list */
  setupInitialLevelAsteroidList(){
    console.log('setup asteroid list');
    // empty list
    this.SET.astlst=[];
    // create new list (1-n)
    for(var x=1;x<(this.SET.astamt+this.SET.plrlvl);x++){
      // create data and set asteroid
      var arr=this.createAsteroidData(this.getRandomAsteroidByType('x'),null);
      this.SET.astlst=this.TLS.addItemToArray(arr,this.SET.astlst);
    }
  }

  /* split asteroid */
  splitAsteroid(obj){
    // get data from object
    var from=obj.getAttribute('attr-key');
    var type=obj.getAttribute('attr-type');
    // what is the next smaller type
    var newtype='';
    switch(type){
      case 'x':
        newtype='m';
        break;
      case 'm':
        newtype='s';
        break;
    }
    // only x->m and m->s
    if(newtype!=''){
      // how many (1-3) new should spawn
      var amt=Math.floor(Math.random()*3)+1;
      // create new asteroids
      this.createNewAsteroids(from,newtype,amt);
    }
    // remove original asteriod
    this.SET.astlst=this.TLS.removeItemFromArrayByKey(from,this.SET.astlst);
  }

  /* create new asteroids (from,type,amount) */
  createNewAsteroids(from,type,amt){
    // create two new asteroids with this type
    if(this.SET.astlst.hasOwnProperty(from)){
      for(var x=1;x<=amt;x++){
        // create data and set asteroid
        var arr=this.createAsteroidData(this.getRandomAsteroidByType(type),from);
        this.SET.astlst=this.TLS.addItemToArray(arr,this.SET.astlst);
      }  
    }
  }

  /* get rotation angle and rotation speed */
  getDegAndRotationSpeed(){
    // rotation
    var deg=Math.random()*360;
    // rotation speed
    var spd=0.25+Math.random()*1.25;
    // rotation direction
    if(Math.random()<0.5){spd*=-1;}
    // return as array
    return [deg,spd];
  }

  /* get random asteroid from list by type */
  getRandomAsteroidByType(type){
    // random item from list
    var num=Math.floor(Math.random()*this.SET.astpool[0][type].length);
    return this.SET.astpool[0][type][num];
  }

  /* get random speeds for asteroid */
  getRandomAsteroidSpeed(){
    // generate random speed (x,y)
    var sx=0.5+(Math.random()*(this.SET.plrlvl/4));
    var rnd=Math.random();
    if(rnd<0.5){sx*=-1;}
    var sy=0.5+(Math.random()*(this.SET.plrlvl/4));
    var rnd=Math.random();
    if(rnd<0.5){sy*=-1;}
    // return speeds
    return [sx,sy];
  }

  /* get mirror position for fly thru */
  getMirrorPosition(val,xy){
    // mirror screen position x/y
    if(xy=='x'){
      if(val<-150){val=this.SET.winw+30;}
      if(val>(this.SET.winw+30)){val=-150;}
    }else{
      if(val<-150){val=this.SET.winh+30;}
      if(val>(this.SET.winh+30)){val=-150;}
    }
    // return new position
    return val;
  }

  /* get speeds (x,y 0-1) for direction angle */
  getSpeedsForAngle(deg){
    // corrections because of css->math
    deg*=-1;
    deg+=90;
    var cor=0;
    if(deg<=0){cor=360;}
    deg=Math.abs(cor-Math.abs(deg%360));
    if(deg>=360){deg=0;}
    // get speeds for angle
    var rad=this.TLS.convertDegToRad(deg);
    var spx=Math.cos(rad);
    var spy=Math.sin(rad);
    spy*=-1;
    // slower for mobile
    if(this.SET.winw<=960){
      spx*=0.75;
      spy*=0.75;
    }
    // result array
    return [spx,spy];
  }



  /* move ship */
  moveShip(){
    // only if game is active
    if(this.SET.run==true){
      // add speed to x/y
      this.SET.shppx+=this.SET.shpspdx;
      this.SET.shppy+=this.SET.shpspdy;
      // come back on the mirrored (x/y) side
      this.SET.shppx=this.getMirrorPosition(this.SET.shppx,'x');
      this.SET.shppy=this.getMirrorPosition(this.SET.shppy,'y');
    }
  }

  /* find a free position for ship */
  setShipPosition(){
    console.log('setup ship position');
    // stop moving
    this.SET.shpspdx=0;
    this.SET.shpspdy=0;
    // get asteroid object list
    var objs=document.querySelectorAll('#rocks>.rock');
    // find new position until area is free
    while(true){
      // random position around screen center
      var px=(Math.random()*(this.SET.winw-400))+200;
      var py=(Math.random()*(this.SET.winh-400))+200;
      // set area to this position as center
      this.TLS.setPositonForElementById('area',(px+12)-(this.SET.spwnsz/2),(py+18)-(this.SET.spwnsz/2));
      // setup
      var spwn=true;
      // check with all asteroids
      var key1=0;
      while(key1<objs.length){
        // is it still in array
        if(objs.hasOwnProperty(key1)){
          // get object
          var obj1=objs[key1];
          // check collision
          if(this.doCollide(document.querySelectorAll('#area')[0],obj1)==true){
            spwn=false;
            break;
          }
        }
        key1++;
      }
      // if area is free, set ship position
      if(spwn==true){
        this.SET.shppx=px;
        this.SET.shppy=py;
        setTimeout(()=>{this.showShip()},this.SET.shptmr);
        break;
      }
    }
  }

  /* increase ship speed by rotation angle */
  increaseShipSpeed(){
    // depending on ship rotation
    var spds=this.getSpeedsForAngle(this.SET.shpdeg);
    this.SET.shpspdx+=spds[0]/5;
    this.SET.shpspdy+=spds[1]/5;
    // limit speed
    if(this.SET.shpspdx>this.SET.shpspdlimit){this.SET.shpspdx=this.SET.shpspdlimit;}
    if(this.SET.shpspdx<this.SET.shpspdlimit*-1){this.SET.shpspdx=this.SET.shpspdlimit*-1;}
    if(this.SET.shpspdy>this.SET.shpspdlimit){this.SET.shpspdy=this.SET.shpspdlimit;}
    if(this.SET.shpspdy<this.SET.shpspdlimit*-1){this.SET.shpspdy=this.SET.shpspdlimit*-1;}
  }

  /* draw ship */
  drawShip(){
    // set rotation
    this.TLS.setRotationForElementById('plr',this.SET.shpdeg);
    // set position
    this.TLS.setPositonForElementById('plr',this.SET.shppx,this.SET.shppy);
    // display ship
    this.TLS.setElementInnerHtmlById('plr',this.getShipHtml());
  }

  /* hide ship */
  hideShip(){
    this.TLS.removeClassFromElementById('plr','show-elem');
  }

  /* show ship */
  showShip(){
    this.TLS.addClassToElementById('plr','show-elem');
  }

  /* ship html */
  getShipHtml(){
    var html='';
    html+='  <svg class="ship" aspect-ratio="XminYmin">'+this.SET.nln;
    html+='    <polygon points="17 24,27 34,17 4,8 32"></polygon>'+this.SET.nln;
    html+='  </svg>'+this.SET.nln;
    return html;
  }



  /* add shots to list (for player or ufo) */
  addShotToList(which){
    // there is a maximum shot amount
    if((this.SET.shtlst.length<this.SET.shtmax)&&(this.SET.shtdel==0)){
      // setup
      var px=0;
      var py=0;
      // player / ufo
      switch(which){
        case 'plr':
          // position
          px=this.TLS.getXPositionForElementById('plr');
          py=this.TLS.getYPositionForElementById('plr');
          // depending on ship rotation
          var spds=this.getSpeedsForAngle(this.SET.shpdeg);
          var sx=spds[0];
          var sy=spds[1];
          // shot from the tip of the ship
          var w=this.TLS.getSizeForElement('plr','x');
          var h=this.TLS.getSizeForElement('plr','y');
          px+=parseInt((w*sx)+(w/2));
          py+=parseInt((h*sy)+(h/2));
          break;
      }
      // add entry (with fast shots)
      if(px!=0&&py!=0){
        var arr=[px,py,(sx*4)+this.SET.shpspdx,(sy*4)+this.SET.shpspdy,this.SET.shtrng];
        this.SET.shtlst=this.TLS.addItemToArray(arr,this.SET.shtlst);
      }
      // set shot delay for next shot
      this.SET.shtdel=this.SET.shtdeldef;
    }
  }

  /* update shot data (for player or ufo) */
  updateShotData(){
    // get data from list for key
    var key=0;
    this.SET.shtlst.forEach((data)=>{
      if(this.SET.shtlst.hasOwnProperty(key)){
        // valid shot (timer>0, px/py on screen)
        var tmr=(data[4]>0)?true:false;
        var pos=((data[0]||data[1]>0)&&(data[0]<this.SET.winw||data[1]<this.SET.winh))?true:false;
        if(tmr&&pos){
          // add speed (px,py,sx,sy,tmr)
          data[0]+=data[2];
          data[1]+=data[3];
          // come back on the mirrored (x/y) side
          data[0]=this.getMirrorPosition(data[0],'x');
          data[1]=this.getMirrorPosition(data[1],'y');
          // reduce timer
          data[4]--;
          // write back to array
          this.SET.shtlst[key]=data;
        }else{
          this.SET.shtlst=this.TLS.removeItemFromArrayByKey(key,this.SET.shtlst);
        }
      }
      key++;
    });
  }

  /* move shots for player and ufo */
  moveShots(){
    // only if game is active
    if(this.SET.run==true){
      // move shots
      this.updateShotData();
      // reduce shot delay
      if(this.SET.shtdel>0){this.SET.shtdel--;}
    }
  }

  /* draw shots (player and ufo) */
  drawShots(){
    // new html
    var html='';
    var key=0;
    this.SET.shtlst.forEach((data)=>{
      html+='    <div class="shot" style="top:'+parseInt(data[1])+'px;left:'+parseInt(data[0])+'px;" attr-key="'+key+'"></div>'+this.SET.nln;
      key++;
    });
    // set html
    this.TLS.setElementInnerHtmlById('shots',html);  
  }



  /* add points from object */
  addPointsFromObject(obj){
    var pts=parseInt(this.TLS.getElementInnerHtmlById('points'));
    pts+=parseInt(obj.getAttribute('attr-pts'));
    this.SET.plrpts=pts;
  }

  /* draw points */
  drawPoints(){
    this.TLS.setElementInnerHtmlById('points',this.TLS.numFormat(this.SET.plrpts,8));
  }



  /* level up */
  addLevel(){
    this.SET.plrlvl++;
  }

  /* draw level */
  drawLevel(){
    this.TLS.setElementInnerHtmlById('level',this.TLS.numFormat(this.SET.plrlvl,3));
  }



  /* take one life */
  removeLife(){
    this.SET.plrlife--;
  }

  /* add one life */
  addLife(){
    this.SET.plrlife++;
    this.TLS.playSound(this.SND.plrlifeaud);
    if(this.SET.plrlife>this.SET.plrlifemax){this.SET.plrlife=this.SET.plrlifemax;}
  }

  /* draw lifes */
  drawLifes(){
    var html='';
    for(var x=1;x<=this.SET.plrlife;x++){html+=this.getShipHtml();}
    this.TLS.setElementInnerHtmlById('lifes',html);
  }



  /* check collisions */
  checkCollisions(){
    // object lists
    var shts=document.querySelectorAll('#shots>.shot');
    var asts=document.querySelectorAll('#rocks>.rock');
    var shps=document.querySelectorAll('#plr>.ship');
    // shots with asteroids
    shts.forEach((shtobj)=>{
      asts.forEach((astobj)=>{
        if(this.doCollide(shtobj,astobj)){
          var key=shtobj.getAttribute('attr-key');
          this.SET.shtlst=this.TLS.removeItemFromArrayByKey(key,this.SET.shtlst);
          this.addPointsFromObject(astobj);
          this.TLS.playSound(this.SND.expaud);
          this.splitAsteroid(astobj);
        }
      });
    });
    // ship with asteroids
    if(this.TLS.isElementVisibleById('plr')){
      shps.forEach((shpobj)=>{
        asts.forEach((astobj)=>{
          if(this.doCollide(shpobj,astobj)){
            this.TLS.playSound(this.SND.expaud);
            this.splitAsteroid(astobj);
            this.removeLife();
            this.SET.con=(this.SET.plrlife>0)?true:false;
            this.stopGame();
            this.showGameInfo();
          }
        });
      });
    }
  }

  /* do two object collide */
  doCollide(obj1,obj2) {
    if((typeof obj1==='object')&&(typeof obj2==='object')){
      // object bounderies
      var one=obj1.getBoundingClientRect();
      var two=obj2.getBoundingClientRect();
      // check collision
      if(  one.left < two.left      + two.width-10 
        && one.left + one.width-10  > two.left 
        && one.top  < two.top       + two.height-10 
        && one.top  + one.height-10 > two.top) {
          return true;
      }
    }
    return false;
  }



  /* update screen */
  updateScreen(){
    // update objects
    this.drawAsteroids();
    this.drawShip();
    this.drawShots();
    // update data
    this.drawPoints();
    this.drawLevel();
    this.drawLifes();
  }



  /* start game */
  startGame(){
    console.log('start game');
    // if there are lives left
    if(this.SET.plrlife>0){
      this.TLS.removeClassFromElementById('start','show-elem');
      if(this.SET.con==false){
		this.addLevel();
        this.setupInitialLevelAsteroidList();
	  }
      this.SET.shpdeg=0;
      this.SET.con=false;
      this.SET.run=true;
      setTimeout(()=>{this.setShipPosition()},250);
    }
  }

  /* stop game */
  stopGame(){
    console.log('stop game');
    this.SET.run=false;
    this.SET.shtlst=[];
    this.hideShip();
  }

  /* setup game */
  setupGame(){
    console.log('setup game');
    // reset player
    this.SET.plrpts=0;
    this.SET.plrlife=this.SET.plrlifedef;
    this.SET.plrlvl=0;
    // reset ship
    this.SET.shpspdx=0;
    this.SET.shpspdy=0;
    this.SET.shpdeg=0;
    // different amounts for different screens
    if(this.SET.winw<1024){this.SET.astamtdef=2;}
    if(this.SET.winw>1600){this.SET.astamtdef=6;}
    // setup asteroids with default object amout
    this.SET.astamt=this.SET.astamtdef;
    this.setupInitialLevelAsteroidList();
    // update everything
    this.updateScreen();
    // no pause
    this.SET.brk=false;
  }

  /* show game info */
  showGameInfo(){
    this.TLS.removeClassFromElementById('start','show-elem');
    this.TLS.removeClassFromElementById('gameover','show-elem');
    var elmid='start';
    if(this.SET.plrlife==0){elmid='gameover';}
    setTimeout(()=>{this.TLS.addClassToElementById(elmid,'show-elem')},250);
  }

  /* check for win/lose game */
  checkGameStatus(){
    // stop game (no lives or no asteroids)
    if(this.SET.run==true&&this.SET.astlst.length==0){
      this.stopGame();
      if(this.SET.plrlife>0){this.addLife();}
      this.showGameInfo();
    }
  }



  /* init game */
  init(){
    console.log('init');
    // setup basics
    this.SET.winw=window.innerWidth;
    this.SET.winh=window.innerHeight;
    // set spawn area size
    this.TLS.setSizeForElementById('area',this.SET.spwnsz,this.SET.spwnsz);
    // center start info
    this.TLS.centerInfoBoxes();
    // setup game params
    this.setupGame();
    // show gameinfo
    this.showGameInfo();
    // hide cover
    this.TLS.hideCover();
    // start game mover
    this.gameMotor();
    // and go
    console.log('ready!');
  }



  /* start game motor */
  gameMotor(){
    setInterval(()=>{
      // if game is not paused
      if(this.SET.brk==false){
        this.moveAsteroids();
        if(this.SET.run==true){
          this.checkCollisions();
          this.checkGameStatus();
          this.moveShots();
          this.moveShip();
        }
        this.updateScreen();
      }
    },25);
  }



  /* handle listener events */
  handleEvent(event) {
    event.preventDefault();
    switch(event.type) {
      case 'click':
        this.leftClickEvent(event);
        break;
      case 'keydown':
        this.keyEvent(event);
        break;
      case 'contextmenu':
        this.rightClickEvent(event);
        break;
      case 'wheel':
        this.wheelEvent(event);
        break;
      case 'resize':
        this.resizeEvent(event);
        break;
    }
  }

  /* left mouse click */
  leftClickEvent(event){
    // generate keyboard events (start,reset,shoot)
    if(GME.SET.run==false){
      if(GME.TLS.isElementVisibleById('start')){
        window.dispatchEvent(new KeyboardEvent('keydown',{'code':'KeyS'}));
      }
      if(GME.TLS.isElementVisibleById('gameover')){
        window.dispatchEvent(new KeyboardEvent('keydown',{'code':'KeyR'}));
      }
    }else{
      window.dispatchEvent(new KeyboardEvent('keydown',{'code':'Space'}));
    }
  }

  /* right mouse click */
  rightClickEvent(event){
    // generate keyboard events
    window.dispatchEvent(new KeyboardEvent('keydown',{'code':'ArrowUp'}));
  }

  /* mouse wheel event */
  wheelEvent(event){
    // get and set the wheel as ship rotation
    GME.SET.shpdeg-=Math.floor(event.deltaY/8);
  }

  /* key press event */
  keyEvent(event){
    switch(event.code){
      // s(tart)
      case 'KeyS':
        GME.TLS.playSound(GME.SND.btnaud);
        if(GME.SET.run==false){GME.startGame();}
        break;
      // r(eset)
      case 'KeyR':
        GME.TLS.playSound(GME.SND.btnaud);
        GME.stopGame();
        GME.setupGame();
        GME.showGameInfo();
        break;
      // p(ause)
      case 'KeyP':
        if(GME.SET.run==true){
          GME.TLS.playSound(GME.SND.btnaud);
          GME.SET.brk=!GME.SET.brk;
        }
        break;
      // thrust (up/w)
      case 'KeyW':
      case 'ArrowUp':
        if(GME.SET.run==true&&GME.SET.brk==false){
          GME.SND.shpaud.currentTime=0;
          GME.TLS.playSound(GME.SND.shpaud);
          GME.increaseShipSpeed();
        }
        break;
      // left (left/a)
      case 'KeyA':
      case 'ArrowLeft':
        if(GME.SET.run==true&&GME.SET.brk==false){GME.SET.shpdeg-=GME.SET.shpdegspd;}
        break;
      // right (right/d)
      case 'KeyD':
      case 'ArrowRight':
        if(GME.SET.run==true&&GME.SET.brk==false){GME.SET.shpdeg+=GME.SET.shpdegspd;}
        break;
      // shoot (space)
      case 'Space':
        if(GME.TLS.isElementVisibleById('plr')&&GME.SET.brk==false){
          if(GME.SET.shtdel==0){GME.TLS.playSound(GME.SND.shtaud);}
          GME.addShotToList('plr');
        }
        break;
      // hyperspace (x)
      case 'KeyX':
        if(GME.TLS.isElementVisibleById('plr')&&GME.SET.brk==false){
          GME.TLS.playSound(GME.SND.hypaud);
          GME.hideShip();
          setTimeout(()=>{GME.setShipPosition()},250);
        }
        break;
    }
  }

  /* window resize event */
  resizeEvent(event){
    // stop and restart game
    GME.stopGame();
    GME.init();
  }
}



/* wait until document is fully loaded and ready to start */
var stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    GME=new Game();
    GME.init();
  }
},250);